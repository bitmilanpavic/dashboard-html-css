(function(){

    const filter_mobile_menu = document.querySelector('.sidebar-right__list-types-container');
    const sidebar_right = document.querySelector('.sidebar-right');
    const main_content = document.querySelector('.main-content');
    const filter_desktop_menu = document.querySelector('.sidebar-left__navigation>li:last-child');

    document.getElementById('filter-button').addEventListener('click', (event) => {
        event.preventDefault();
        filter_mobile_menu.classList.toggle('hide');
        sidebar_right.classList.toggle('hide');
        main_content.classList.toggle('wide');
        filter_desktop_menu.classList.toggle('hide');
    });

    filter_desktop_menu.addEventListener('click', (event) => {
        event.preventDefault();
        main_content.classList.toggle('wide');
        sidebar_right.classList.toggle('hide');
        filter_desktop_menu.classList.toggle('hide');
    })

}());

